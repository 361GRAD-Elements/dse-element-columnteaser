<?php

/**
 * 361GRAD Element Columnteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_columnteaser'] = ['Column Teaser', 'Column Teaser.'];

$GLOBALS['TL_LANG']['tl_content']['dse_text']      = ['Text', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_ctaTitle']  = ['CTA Button Titel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref']   = ['CTA Button Link', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_bgImage']   = ['Background Image', ''];
$GLOBALS['TL_LANG']['tl_content']['columnteaser_legend']   = 'Teaser settings';