<?php

/**
 * 361GRAD Element Columnteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_columnteaser'] =
    'Dse\\ElementsBundle\\ElementColumnteaser\\Element\\ContentDseColumnteaser';

// Cascading Style Sheets
$GLOBALS['TL_CSS']['dse_columnteaser'] = 'bundles/dseelementcolumnteaser/css/fe_ce_dse_columnteaser.css';

