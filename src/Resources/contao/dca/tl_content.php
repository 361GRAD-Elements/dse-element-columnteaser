<?php

/**
 * 361GRAD Element Columnteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_columnteaser'] =
    '{type_legend},type,headline;' .
    '{columnteaser_legend},dse_text,dse_ctaHref,dse_ctaTitle,dse_bgImage;' .
    '{invisible_legend:hide},invisible,start,stop';

// ELement fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_text']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_text'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => true,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_ctaHref'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_ctaTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_ctaTitle'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_bgImage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_bgImage'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class' => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
